#
msgid ""
msgstr "Content-Type: text/plain; charset=utf-8\n"

msgctxt "field:asset,attribute_set:"
msgid "Set"
msgstr "Conjunt"

msgctxt "field:asset,attributes:"
msgid "Attributes"
msgstr "Atributs"

msgctxt "field:asset.attribute,create_date:"
msgid "Create Date"
msgstr "Data creació"

msgctxt "field:asset.attribute,create_uid:"
msgid "Create User"
msgstr "Usuari creació"

msgctxt "field:asset.attribute,digits:"
msgid "Digits"
msgstr "Dígits"

msgctxt "field:asset.attribute,id:"
msgid "ID"
msgstr "ID"

msgctxt "field:asset.attribute,name:"
msgid "Name"
msgstr "Nom"

msgctxt "field:asset.attribute,rec_name:"
msgid "Name"
msgstr "Nom"

msgctxt "field:asset.attribute,selection:"
msgid "Selection"
msgstr "Selecció"

msgctxt "field:asset.attribute,selection_json:"
msgid "Selection JSON"
msgstr "JSON selecció"

msgctxt "field:asset.attribute,sets:"
msgid "Sets"
msgstr "Conjunts"

msgctxt "field:asset.attribute,string:"
msgid "String"
msgstr "Etiqueta"

msgctxt "field:asset.attribute,type_:"
msgid "Type"
msgstr "Tipus"

msgctxt "field:asset.attribute,write_date:"
msgid "Write Date"
msgstr "Data modificació"

msgctxt "field:asset.attribute,write_uid:"
msgid "Write User"
msgstr "Usuari modificació"

msgctxt "field:asset.attribute-asset.attribute-set,attribute:"
msgid "Attribute"
msgstr "Atribut"

msgctxt "field:asset.attribute-asset.attribute-set,attribute_set:"
msgid "Set"
msgstr "Conjunt"

msgctxt "field:asset.attribute-asset.attribute-set,create_date:"
msgid "Create Date"
msgstr "Data creació"

msgctxt "field:asset.attribute-asset.attribute-set,create_uid:"
msgid "Create User"
msgstr "Usuari creació"

msgctxt "field:asset.attribute-asset.attribute-set,id:"
msgid "ID"
msgstr "ID"

msgctxt "field:asset.attribute-asset.attribute-set,rec_name:"
msgid "Name"
msgstr "Nom"

msgctxt "field:asset.attribute-asset.attribute-set,write_date:"
msgid "Write Date"
msgstr "Data modificació"

msgctxt "field:asset.attribute-asset.attribute-set,write_uid:"
msgid "Write User"
msgstr "Usuari modificació"

msgctxt "field:asset.attribute.set,attributes:"
msgid "Attributes"
msgstr "Atributs"

msgctxt "field:asset.attribute.set,create_date:"
msgid "Create Date"
msgstr "Data creació"

msgctxt "field:asset.attribute.set,create_uid:"
msgid "Create User"
msgstr "Usuari creació"

msgctxt "field:asset.attribute.set,id:"
msgid "ID"
msgstr "ID"

msgctxt "field:asset.attribute.set,name:"
msgid "Name"
msgstr "Nom"

msgctxt "field:asset.attribute.set,rec_name:"
msgid "Name"
msgstr "Nom"

msgctxt "field:asset.attribute.set,write_date:"
msgid "Write Date"
msgstr "Data modificació"

msgctxt "field:asset.attribute.set,write_uid:"
msgid "Write User"
msgstr "Usuari modificació"

msgctxt "help:asset.attribute,selection:"
msgid "A couple of key and label separated by \":\" per line"
msgstr "Clau i valor separats per \":\" a cada línia."

msgctxt "model:asset.attribute,name:"
msgid "Asset Attribute"
msgstr "Attribut d'actiu"

msgctxt "model:asset.attribute-asset.attribute-set,name:"
msgid "Asset Attribute - Set"
msgstr "Conjunt - attributs d'actius"

msgctxt "model:asset.attribute.set,name:"
msgid "Asset Attribute Set"
msgstr "Conjunt d'attributs d'actius"

msgctxt "model:ir.action,name:act_attribute_form"
msgid "Attributes"
msgstr "Atributs"

msgctxt "model:ir.action,name:act_attribute_set_form"
msgid "Attribute Sets"
msgstr "Conjunts d'atributs"

msgctxt "model:ir.ui.menu,name:menu_attribute"
msgid "Attributes"
msgstr "Atributs"

msgctxt "model:ir.ui.menu,name:menu_attribute_set"
msgid "Attribute Sets"
msgstr "Conjunts d'atributs"

msgctxt "selection:asset.attribute,type_:"
msgid "Boolean"
msgstr "Booleà"

msgctxt "selection:asset.attribute,type_:"
msgid "Char"
msgstr "Text"

msgctxt "selection:asset.attribute,type_:"
msgid "Date"
msgstr "Data"

msgctxt "selection:asset.attribute,type_:"
msgid "DateTime"
msgstr "Data/Hora"

msgctxt "selection:asset.attribute,type_:"
msgid "Float"
msgstr "Número coma flotant"

msgctxt "selection:asset.attribute,type_:"
msgid "Integer"
msgstr "Enter"

msgctxt "selection:asset.attribute,type_:"
msgid "Numeric"
msgstr "Númeric"

msgctxt "selection:asset.attribute,type_:"
msgid "Selection"
msgstr "Selecció"

msgctxt "view:asset.attribute.set:"
msgid "Attribute Set"
msgstr "Conjunt d'atributs"

msgctxt "view:asset.attribute.set:"
msgid "Attribute Sets"
msgstr "Conjunts d'atributs"

msgctxt "view:asset.attribute:"
msgid "Attribute"
msgstr "Atribut"

msgctxt "view:asset.attribute:"
msgid "Attributes"
msgstr "Atributs"
